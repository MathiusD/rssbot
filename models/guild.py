from own.own_sqlite.models import *
from own.own_sqlite.fields import *
from own.own_sqlite.keys import *
import sqlite3

class Guild:

    def __init__(self, db:sqlite3.Connection):
        fields = [
            Id,
            Integer("guild", True),
            Integer("notifChan"),
            Text("prefix")
        ]
        self.db = db
        self.model = Model("Guild", fields=fields, primary_key=Default_PrimaryKey, db=db)

    def addGuild(self, guild:int, chan:int = None, prefix:str = None):
        if len(self.model.fetchByField(guild, self.model.fields[1], self.db)) == 0:
            data = ModelData(self.model.fields, [None, guild, chan, prefix])
            return self.model.insertOne(data, self.db)
        return None

    def updateGuild(self, guild:int, chan:int = None, prefix:str = None, verbose:bool = False):
        data = self.model.fetchByField(guild, self.model.fields[1], self.db)
        if data:
            for dat in data:
                dat.notifChan = chan if chan else dat.notifChan
                dat.prefix = prefix if prefix else dat.prefix
                dat.Guild = guild
            return self.model.updateMany(data, self.db, verbose)
        return None

    def removeOptionsofGuild(self, guild:int, attr:str, verbose:bool = False):
        data = self.model.fetchByField(guild, self.model.fields[1], self.db)
        if data:
            for dat in data:
                setattr(dat, attr, None)
                dat.Guild = guild
            return self.model.updateMany(data, self.db, verbose)
        return None

    def fetchGuildByDiscordId(self, guild:int):
        return self.model.fetchOneByField(guild, self.model.fields[1], self.db)

    def fetchGuildId(self, guildId:int):
        return self.model.fetchOneByField(guildId, self.model.fields[0], self.db)