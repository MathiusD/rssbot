from own.own_sqlite.models import *
from own.own_sqlite.fields import *
from own.own_sqlite.keys import *
from .guild import Guild
from .flux import Flux
import sqlite3

class FluxGuild:

    def __init__(self, db:sqlite3.Connection, guilds:Guild, fluxs:Flux):
        self.fluxs = fluxs
        self.guilds = guilds
        fields = [
            Id,
            Integer("flux", True),
            Integer("guild", True),
            Text("msg")
        ]
        self.db = db
        foreign_key = [
            ForeignKey(fields[2], FieldRef(Id, self.guilds.model.name)),
            ForeignKey(fields[1], FieldRef(Id, self.fluxs.model.name))
        ]
        self.model = Model("FluxGuild", fields=fields, primary_key=Default_PrimaryKey, db=db, foreign_key=foreign_key)

    def addFlux(self, guildId:int, fluxId:int, msg:str = None):
        guild = self.guilds.fetchGuildByDiscordId(guildId)
        if guild is None:
            guild = self.guilds.addGuild(guildId)
        flux = self.fluxs.fetchFlux(fluxId)
        if guild is not None and flux is not None:
            data = ModelData(self.model.fields, [None, flux.id, guild.id, msg])
            return self.model.insertOne(data, self.db)
        return None

    def fetchFluxUsed(self):
        fluxsUsed = []
        fluxsAvailable = self.fluxs.fetchAll()
        for flux in fluxsAvailable:
            guilds = self.fetchGuildForFlux(flux.id)
            if guilds is not None and len(guilds) != 0:
                fluxsUsed.append(flux)
        return fluxsUsed

    def fetchGuildForFlux(self, fluxId:int):
        flux = self.fluxs.fetchFlux(fluxId)
        if flux is not None:
            return self.model.fetchByField(flux.id, self.model.fields[1], self.db)
        return None

    def fetchFluxByGuild(self, guildId:int):
        guild = self.guilds.fetchGuildByDiscordId(guildId)
        if guild is not None:
            return self.model.fetchByField(guild.id, self.model.fields[2], self.db)
        return None

    def removeFluxForGuild(self, fluxId:int, guildId:int):
        guild = self.guilds.fetchGuildByDiscordId(guildId)
        flux = self.fluxs.fetchFlux(fluxId)
        if guild is not None and flux is not None:
            return self.model.DelByFields(
                [guild.id, flux.id],
                [
                    self.model.fields[2],
                    self.model.fields[1]
                ],
                self.db
            )
        return None