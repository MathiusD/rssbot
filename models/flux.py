from own.own_sqlite.models import *
from own.own_sqlite.fields import *
from own.own_sqlite.keys import *
import sqlite3, time

class Flux:

    def __init__(self, db:sqlite3.Connection):
        fields = [
            Id,
            TextWithoutCaseSensitive("target", True),
            Real("lastConsult", True)
        ]
        self.db = db
        self.model = Model("Flux", fields=fields, primary_key=Default_PrimaryKey, db=db)

    def addFlux(self, target:str):
        if self.model.fetchOneByField(target, self.model.fields[1], self.db) is None:
            return self.model.insertOne(ModelData(self.model.fields, [None, target, time.time()]), self.db)
        return None

    def updateLastConsult(self, fluxId:int):
        timestamp = time.time()
        data = self.model.fetchOneByField(fluxId, self.model.fields[0], self.db)
        if data is not None:
            data.lastConsult = timestamp
            return self.model.updateOne(data, self.db)
        return None

    def fetchFlux(self, fluxId:int):
        return self.model.fetchOneByField(fluxId, self.model.fields[0], self.db)

    def fetchFluxByTarget(self, target:str):
        return self.model.fetchOneByField(target, self.model.fields[1], self.db)

    def removeFlux(self, fluxId:int):
        return self.model.DelByField(fluxId, self.model.fields[0], self.db)

    def fetchAll(self):
        return self.model.fetchAll(self.db)