from discord.ext import commands
from .cogDb import CogRSS
from own import own_discord
import re, feedparser

class RSS(CogRSS):

    @commands.command()
    @commands.guild_only()
    @commands.has_guild_permissions(administrator=True)
    async def addFlux(self, ctx:commands.Context, uri:str, *, msg:str = None):
        regexUri = "(<)?http(s)?://.*(>)?"
        outMsg = None
        uri = uri[1:] if uri.startswith("<") else uri
        uri = uri[:-1] if uri.endswith(">") else uri
        if uri is None or re.match(regexUri, uri) is None:
            raise commands.UserInputError("Uri is required")
        flux = self.fluxs.fetchFluxByTarget(uri)
        if flux is None:
            if re.match("rss.*", feedparser.parse(uri)['version']) is not None:
                flux = self.fluxs.addFlux(uri)
        if flux is not None:
            if self.fluxGuilds.addFlux(ctx.guild.id, flux.id, msg) is not None:
                outMsg = "Flux add !"
                guildData = self.guilds.fetchGuildByDiscordId(ctx.guild.id)
                if guildData is None or guildData.notifChan is None:
                    outMsg = "%s For send Flux you must add a notifyChan for that (with `notifyChan` command)" % outMsg
            else:
                outMsg = "Flux is already set for this guild !"
        await own_discord.reponse_send(ctx, outMsg if outMsg is not None else "Wrong Uri for RSS Flux")