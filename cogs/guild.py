from discord.ext import commands
from .cogDb import CogGuild
from own import own_discord
from discord import TextChannel

class Guild(CogGuild):

    @commands.command()
    @commands.guild_only()
    @commands.has_guild_permissions(administrator=True)
    async def prefix(self, ctx:commands.Context, prefix:str = None):
        guild = self.guilds.fetchGuildByDiscordId(ctx.guild.id)
        if prefix is not None:
            if guild is not None:
                update = self.guilds.updateGuild(ctx.guild.id, prefix=prefix)
                if update is True:
                    await own_discord.reponse_send(ctx, "New prefix set !")
                else:
                    raise AttributeError("Error on set prefix ! (Update Value = %s)" % update)
            else:
                add = self.guilds.addGuild(ctx.guild.id, prefix=prefix)
                if add is not None:
                    await own_discord.reponse_send(ctx, "New prefix set !")
                else:
                    raise AttributeError("Error on set prefix ! (Add Value = %s)" % add)
        else:
            if guild is not None and guild.prefix is not None:
                await own_discord.reponse_send(ctx, guild.prefix)
            else:
                await own_discord.reponse_send(ctx, "No prefix set.")

    @commands.command()
    @commands.guild_only()
    @commands.has_guild_permissions(administrator=True)
    async def notifyChan(self, ctx:commands.Context, chan:TextChannel = None):
        guild = self.guilds.fetchGuildByDiscordId(ctx.guild.id)
        if chan is not None:
            if guild is not None:
                update = self.guilds.updateGuild(ctx.guild.id, chan=chan.id)
                if update is True:
                    await own_discord.reponse_send(ctx, "New chan set !")
                else:
                    raise AttributeError("Error on set chan ! (Update Value = %s)" % update)
            else:
                add = self.guilds.addGuild(ctx.guild.id, chan=chan.id)
                if add is not None:
                    await own_discord.reponse_send(ctx, "New chan set !")
                else:
                    raise AttributeError("Error on set chan ! (Add Value = %s)" % add)
        else:
            if guild is not None and guild.notifChan is not None:
                await own_discord.reponse_send(ctx, "<#%s>" % guild.notifChan)
            else:
                await own_discord.reponse_send(ctx, "No chan set.")