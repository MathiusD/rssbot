from discord.ext import commands
import models

class CogGuild(commands.Cog):

    def __init__(self, guilds:models.Guild):
        self.guilds = guilds

class CogRSS(CogGuild):
    
    def __init__(self, fluxGuilds:models.FluxGuild):
        self.fluxGuilds = fluxGuilds
        self.fluxs = self.fluxGuilds.fluxs
        super().__init__(self.fluxGuilds.guilds)