from discord.ext import commands
from models import FluxGuild
import discord, logging
from own import own_discord

async def sendFlux(bot:commands.AutoShardedBot, fluxguilds:FluxGuild, fluxId:int, coro):
    for guildData in fluxguilds.fetchGuildForFlux(fluxId):
        guildDb = fluxguilds.guilds.fetchGuildId(guildData.guild)
        if guildDb and guildDb.notifChan:
            guild = await bot.fetch_guild(guildDb.guild)
            channel = bot.get_channel(guildDb.notifChan)
            if channel:
                try:
                    first = False
                    msg = await channel.history(limit=1).flatten()
                    if len(msg) == 0:
                        msg = await channel.send("Sending...")
                        first = True
                    else:
                        msg = msg[0]
                    await coro(await bot.get_context(msg), guildData.msg)
                    if first is True:
                        await msg.delete()
                except discord.Forbidden:
                    chan = await own_discord.getFirstChanWithAllowSendMsg(guild, bot)
                    if chan:
                        warn = "The bot <@%i> will not be allowed to send a message in the notification lounge <#%i>" % (bot.user.id, channel.id)
                        await chan.send(warn)
                    logging.info("In guild : %s (%i), channel : %s (%i) isn't allow send message" % (guild, guild.id, channel, channel.id))