from own.own_sqlite.keys import key
from own.own_discord.msg.msg import embed_send
import time, logging, discord, traceback, cogs, asyncio, feedparser, datetime, utils
from discord.ext import commands
from config import settings
from own import own_discord, own_sqlite
from models import Flux, Guild, FluxGuild

logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s', filename='logs/%s.log'%(str(time.time())), level=settings.DISCORD_LOG)

logging.info("Verification Database")
if (own_sqlite.bdd_exist(settings.DISCORD_DB) != True):
    logging.info("Creating Database")
    own_sqlite.bdd_create(settings.DISCORD_DB)

logging.info("Connecting Database")
bdd = own_sqlite.connect(settings.DISCORD_DB)

guilds = Guild(bdd)
fluxs = Flux(bdd)
fluxGuilds = FluxGuild(bdd, guilds, fluxs)

def determine_prefix(bot:commands.AutoShardedBot, message:discord.Message):
    prefix = None
    if message.guild:
        guild = guilds.fetchGuildByDiscordId(message.guild.id)
        prefix = guild.prefix if guild is not None else None
    return settings.DISCORD_PREFIX if not prefix else prefix

bot = commands.AutoShardedBot(command_prefix=determine_prefix, case_insensitive=True)

bot.add_cog(cogs.RSS(fluxGuilds))
bot.add_cog(cogs.Guild(guilds))

@bot.event
async def on_ready():
    act = discord.Game(settings.DISCORD_HELP)
    await bot.change_presence(activity=act)
    logging.info("Bot Launch.")
    print("Bot Up !")

async def waitingStart(bot:commands.AutoShardedBot):
    while bot.is_ready() == False:
        await asyncio.sleep(5)

async def checkFlux(bot:commands.AutoShardedBot, loop:bool = True):
    while loop is True:
        for flux in fluxGuilds.fetchFluxUsed():
            fluxs.updateLastConsult(flux.id)
            latchFetch = datetime.datetime.fromtimestamp(flux.lastConsult)
            fluxData = feedparser.parse(flux.target, modified=latchFetch)
            sendEntries = []
            if "entries" in fluxData.keys():
                for entry in fluxData["entries"]:
                    publishedParsed = datetime.datetime.fromtimestamp(time.mktime(entry["published_parsed"]))
                    if latchFetch < publishedParsed:
                        sendEntries.append(entry)
            title = flux.target
            if "feed" in fluxData.keys():
                if "title" in fluxData["feed"].keys():
                    title = fluxData["feed"]["title"]
            if len(sendEntries) != 0:
                sendEmbeds = []
                for entry in sendEntries:
                    sendEmbeds.append(
                        own_discord.embed_create(
                            "%s%s%s" % (
                                "Summary:\n%s" % entry["summary"] if "summary" in entry.keys() else "",
                                "\n" if "summary" in entry.keys() and "link" in entry.keys() else "",
                                "Link:\n%s" % entry["link"] if "link" in entry.keys() else ""
                            ),
                            "%s%s" % (
                                title,
                                "-%s" % (entry["title"]) if "title" in entry.keys() else ""
                            )
                        )
                    )
                async def sendFlux(ctx:commands.Context, msg:str = None):
                    for embeds in sendEmbeds:
                        if msg is not None:
                            await own_discord.reponse_send(ctx, msg)
                        await own_discord.embed_send(ctx, embeds, True)
                await waitingStart(bot)
                await utils.sendFlux(bot, fluxGuilds, flux.id, sendFlux)
        await asyncio.sleep(5 * 60)

async def commandError(ctx:commands.Context, exc:commands.CommandError):
    log = True
    if ctx.command:
        if isinstance(exc, commands.UserInputError):
            log = False
            await own_discord.reponse_send(ctx, "Bad Usage of this command.")
        elif isinstance(exc, commands.MissingPermissions):
            log = False
            await own_discord.reponse_send(ctx, "Missing Permissions for perform this command.")
        elif isinstance(exc, commands.CommandOnCooldown):
            log = False
            await own_discord.reponse_send(ctx, exc.__str__())
        else:
            app = await bot.application_info()
            await own_discord.reponse_send(ctx, "Error as occured please contact <@%i> (%s#%s)." % (app.owner.id, app.owner.name, app.owner.discriminator))
    if log:
        if not isinstance(exc, commands.CommandNotFound):
            logs = traceback.format_exception(type(exc), exc, exc.__traceback__)
            location = "For User %s#%s(%i)" % (ctx.author.name, ctx.author.discriminator, ctx.author.id)
            if ctx.command:
                location = "In use %s %s" % (ctx.command.name, location)
            if ctx.guild:
                location = "%s In Guild \"%s\"(%i)" % (location, ctx.guild.name, ctx.guild.id)
            logs.append(location)
            logs.append("Call : %s" % ctx.message.content)
            for line in logs:
                print(line)
                logging.warning(line)

bot.on_command_error = commandError

bot.loop.create_task(checkFlux(bot))

bot.run(settings.DISCORD_TOKEN)