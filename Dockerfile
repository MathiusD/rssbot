FROM python:latest

WORKDIR /usr/docker/rssbot

COPY . .

RUN make rebuild

CMD ["make", "start"]